const crypto = require('crypto');
//const hash = crypto.createHash('sha-256')

module.exports = {


  friendlyName: 'Hash password',


  description: 'Hash password for safer stroing it in database.',


  inputs: {

    password: {
      type: 'string',
      minLength: 7,
    },

  },


  exits: {

  },


  fn: async function (inputs, exits) {

    const { password } = inputs;

    const hash = crypto.createHash(sails.config.custom.hashingAlogithm)
      .update(password)
      .digest('base64');

    return exits.success(hash);

  }


};

