/**
 * UserController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

module.exports = {

  login: async function(req, res) {

    const username = req.param('username');
    const password = req.param('password');

    const user = await User.findOne({
      username,
    });

    if (!user) {
      return res.notFound();
    }

    await bcrypt.compare(password, user.password);

    const token = jwt.sign(
      { id: user.id },
      sails.config.custom.secretKey,
      { expiresIn: sails.config.custom.jwtExpires },
    );

    return res.json({
      jwt: token,
    });

  },

};

