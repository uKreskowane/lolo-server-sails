/**
 * Custom configuration
 * (sails.config.custom)
 *
 * One-off settings specific to your application.
 *
 * For more information on custom configuration, visit:
 * https://sailsjs.com/config/custom
 */

module.exports.custom = {

  hashingAlogithm: 'sha256',
  secretKey: 'f18r9nyat*&d5bD5rAX(D59a9nasd8a6)B(^F)',
  jwtExpires: (60 * 15) * 100,

};
