/**
 * Product.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    name: {
      type: 'string',
      required: true,
    },

    description: {
      type: 'string',
      defaultsTo: '',
    },

    price: {
      type: 'number',
      required: true,
    },

    images: {
      type: 'json',
      columnType: 'array',
      defaultsTo: [],
    },

    isActive: {
      type: 'boolean',
      defaultsTo: true,
    },

    createdBy: {
      model: 'user',
    },

  },

};
