module.exports = {


  friendlyName: 'Create',


  description: 'Create product.',

  files: ['images'],

  inputs: {

    name: {
      type: 'string',
      required: true,
    },

    description: {
      type: 'string',
      defaultsTo: '',
    },

    price: {
      type: 'number',
      required: true,
    },

    images: {
      type: 'ref',
    },

  },


  exits: {

  },


  fn: async function (inputs, exits) {

    try {
      const product = await Product.create({
        name: inputs.name,
        description: inputs.description,
        price: inputs.price,
      }).fetch();

      inputs.images.upload({
        dirname: require('path').resolve(sails.config.appPath, 'assets/images/products'),
      }, async function (err, images) {
        if (err) {
          return exits.serverError(err);
        }

        const listOfImages = [];

        for (const image of images) {
          const { fd } = image;
          const filename = fd.substr(fd.lastIndexOf('/') + 1);
          listOfImages.push(filename);
        }

        await Product.update(product.id, { images: listOfImages });
      });

      return exits.success();

    } catch (error) {
      return exits.serverError(error);
    }

  },


};
