/**
 * Bootstrap
 * (sails.config.bootstrap)
 *
 * An asynchronous bootstrap function that runs just before your Sails app gets lifted.
 * > Need more flexibility?  You can also do this by creating a hook.
 *
 * For more information on bootstrapping your app, check out:
 * https://sailsjs.com/config/bootstrap
 */

const SAMPLE_DESCRIPTION = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eleifend nunc pharetra tortor iaculis, a porttitor arcu porta. Proin eu laoreet sem. Vivamus et tempor odio. Quisque aliquam lectus eu enim feugiat, id fringilla tortor porttitor. Nam vitae quam hendrerit nulla imperdiet imperdiet eu non odio.';

module.exports.bootstrap = async function(done) {

  let user;

  if (await User.count() < 1) {
    user = await User.create({
      username: 'Me',
      email: 'me@localhost.com',
      password: '1234567',
      images: [],
    }).fetch();
  } else {
    user = await User.findOne({ username: 'Me' });
  }

  const { id } = user;

  if (await Product.count() < 1) {
    await Product.createEach([
      { name: 'Logo', description: SAMPLE_DESCRIPTION, price: 100, isActive: true, createdBy: id },
      { name: 'Mobile app', description: SAMPLE_DESCRIPTION, price: 150, isActive: true, createdBy: id },
      { name: 'Program', description: SAMPLE_DESCRIPTION, price: 1700, isActive: true, createdBy: id },
      { name: 'Poster', description: SAMPLE_DESCRIPTION, price: 70, isActive: true, createdBy: id },
    ]).fetch();
  }

  return done();

};
