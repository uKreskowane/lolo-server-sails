/**
 * User.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    username: {
      type: 'string',
      required: true,
      unique: true,
    },

    email: {
      type: 'string',
      isEmail: true,
      required: true,
    },

    password: {
      type: 'string',
      required: true,
      minLength: 7,
    },

  },

  beforeCreate: async (user, proceed) => {
    try {
      user.password = await sails.helpers.user.hashPassword(user.password);
      return proceed();
    } catch (error) {
      return proceed(error);
    }
  },

  customToJSON: function () {
    return _.omit(this, ['password']);
  },

};

