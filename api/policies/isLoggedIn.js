const jwt = require('jsonwebtoken');

module.exports = async function (req, res, proceed) {

  const authHeader = req.headers['authorization'];

  if (!authHeader) {
    return res.status(401).send();
  }

  const token = authHeader.split('Bearer ')[1];
  const { secretKey } = sails.config.custom;

  jwt.verify(token, secretKey, (error, decoded) => {
    if (error) {
      return res.serverError(error);
    }

    req.me = decoded;

    return proceed();
  });

};
